Import-Module AWSPowerShell.NetCore
$AccessKey  = "access key"
$SecretKey  = "secret key"
$Region     = "us-west-1"
$Sgid= "sg-address"
$snid = "subnet-address"
$InstanceName = "MyInstance"
$InstanceType = 't2.micro'
$BillingKey = "Infrastructure"
$KeyName = "MyAWSKeyPair"

Set-AWSCredential -AccessKey $AccessKey -SecretKey $SecretKey -StoreAs default
Set-DefaultAWSRegion -Region $Region

$AMI = Get-EC2ImageByName -Names 'WINDOWS_2016_BASE'

$tag0 = @{ key="Name"; Value=$InstanceName }
$tag1 = @{ key="Billing"; Value=$BillingKey }
$tagspec1 = new-object Amazon.EC2.Model.TagSpecification
$tagspec1.ResourceType = "instance"
$tagspec1.Tags.Add($tag0)
$tagspec1.Tags.Add($tag1)

$bdm = New-Object Amazon.Ec2.Model.BlockDeviceMapping
$bdm.DeviceName = '/dev/sda1'

$ebd = New-Object Amazon.Ec2.Model.EbsBlockDevice
$ebd.VolumeSize = '50'
$ebd.VolumeType = 'gp2'

$bdm.Ebs = $ebd

$inst1 = New-EC2Instance -ImageId $AMI -MinCount 1 -MaxCount 1 -KeyName $KeyName `
             -SecurityGroupId $Sgid -SubnetId $snid -TagSpecification $tagspec1 `
             -InstanceType $InstanceType -BlockDeviceMapping $bdm
Start-Sleep 60

$reservation = New-Object 'collections.generic.list[string]'
$reservation.add($inst1.ReservationId)
$filter_reservation = New-Object Amazon.EC2.Model.Filter -Property @{Name = "reservation-id"; Values = $reservation}
$instid = (Get-EC2Instance -Filter $filter_reservation).Instances.instanceID

$status = Get-EC2InstanceStatus -InstanceId $instid -Verbose 
$status.Status
while ($status.status.status.value -ne "ok") {
    Start-Sleep 5
    $status = Get-EC2InstanceStatus -InstanceId $instid -Verbose
}
Write-Host ((Get-EC2instance -InstanceId $instid).Instances).PublicIpAddress


#attach IAM Role to Instance.
Register-EC2IamInstanceProfile -InstanceId $instid -IamInstanceProfile_Name 'SSM'
Restart-EC2Instance -InstanceId $instid
$status1 = 'NONE'
Start-Sleep 5
While ($status1.PingStatus.Value -ne 'Online') {
    echo 'Waiting for VM to be Ready'
    $status1 = Get-SSMInstanceInformation -InstanceInformationFilterList @{Key="InstanceIds";ValueSet="$instid"}
    Start-Sleep 5
}





# Create and Attach Log Drive
$sn = Get-EC2Subnet -SubnetId $snid
$tag = @{ Key="Billing"; Value=$envData.Name}
$tagspec = New-Object Amazon.EC2.Model.TagSpecification
$tagspec.ResourceType = 'volume'
$tagspec.Tags.Add($tag)

$vol = New-EC2Volume -Size 20 -AvailabilityZone $sn.AvailabilityZone -TagSpecification $tagspec
$status = Get-EC2Volume -VolumeId $vol.VolumeId 
$status.Status
while ($status.status -ne "available") {
    Start-Sleep 5
    echo 'Waiting for Volume'
    $status.Status
    $status = Get-EC2Volume -VolumeId $vol.VolumeId 
}
$voladd = Add-EC2Volume -InstanceId $instid -VolumeId $vol.VolumeId -Device /dev/sdh


$setdrivecmd = "Get-Disk | Where partitionstyle -eq 'RAW' | Initialize-Disk -PartitionStyle MBR -PassThru | New-Partition -AssignDriveLetter -UseMaximumSize | Format-Volume -FileSystem NTFS -NewFileSystemLabel 'Logs' -Confirm:`$false "
$setdrive = @('import-module AWSPowerShell', $setdrivecmd)
$scriptstat = Send-SSMCommand -InstanceId @($instid) -DocumentName AWS-RunPowerShellScript -Parameter @{'commands'=$setdrive}

$drive = (Get-EC2Instance -InstanceId $instid).Instances.BlockDeviceMappings | Where-Object -FilterScript { $PSItem.DeviceName -match '/dev/sda1' }
$drive.Ebs.VolumeId
New-EC2Tag -Resource $drive.Ebs.VolumeId -Tag @{ Key= 'Billing'; Value=$envData.Name}
# End Attach Log drive