Import-Module AWSPowerShell
Set-AWSCredential -AccessKey $key -SecretKey $secret -StoreAs default
$regionlist = @("us-west-1","us-west-2","us-east-1","us-east-2")

foreach ($region in $regionlist) {
    Set-DefaultAWSRegion -Region $region
    $ec2list = (Get-EC2Instance).Instances
    foreach ($ec2 in $ec2list) {

        #add additional checks here
        # if ($ec2.InstanceType.Value -eq "t3.large") {

        $name =  $ec2 | select @{Name="Servername";Expression={$_.tags | where key -eq "Name" | select Value -expand Value}}
        #Create Cloudwatch Alarms
        Write-Host 'Create Alarms'

        $recoverARN = 'arn:aws:automate:'+$region+':ec2:recover'
        $alertARN = 'arn:aws:sns:'+$region+':acctountno:RECOVER-ALERT'

        $ActionARN = @($recoverARN,$alertARN)

        $dim = New-Object 'Amazon.CloudWatch.Model.Dimension'
        $dim.Name = 'InstanceId'
        $dim.Value = $ec2.InstanceID
        $alarmName = $name.Servername + '-Recover'

        Write-CWMetricAlarm -AlarmName $alarmName `
            -ActionsEnabled $true -AlarmAction $ActionARN `
            -ComparisonOperator 'GreaterThanThreshold' -Dimension $dim `
            -EvaluationPeriod 2 -MetricName 'StatusCheckFailed_System'  -Namespace AWS/EC2 `
            -Period 300 -Statistic Average -TreatMissingData missing -Threshold 0 `
            -DatapointsToAlarm 2 

        # } End Of Additional Check
    }  #end of foreach EC2 loop
}  #end of Region loop