Import-Module AWSPowerShell.NetCore
$AccessKey  = "access-key"
$SecretKey  = "secret-key"
$Region     = "us-east-1"
$vpcCIDR    = "10.0.0.0/23"
$ipasubnet  = "10.0.0.0/24"
$ipbsubnet  = "10.0.1.0/24"
$vpcname    = "DEMOVPC"

Set-AWSCredential -AccessKey $AccessKey -SecretKey $SecretKey -StoreAs default
Set-DefaultAWSRegion -Region $Region

$newvpc = new-ec2vpc -CidrBlock $vpcCIDR   
$vpcstat = Get-EC2Vpc -VpcId $newvpc.VpcId
While ($vpcstat.State -ne "available") {
    $vpcstat = Get-EC2Vpc -VpcId $newvpc.VpcId
}
$tag = New-Object Amazon.EC2.Model.Tag
$tag.key = "Name"
$tag.Value = $vpcname
New-EC2Tag -Resource $newvpc.VpcId -Tag $tag
$tagb = New-Object Amazon.EC2.Model.Tag
$tagb.key = "Billing"
$tagb.Value = "Infrastructure"
New-EC2Tag -Resource $newvpc.VpcId -Tag $tagb

$azlist = Get-EC2AvailabilityZone
$SubnetA = New-EC2Subnet -VpcId $newvpc.VpcId -CidrBlock $ipasubnet -AvailabilityZone $azlist[0].ZoneName
Edit-EC2SubnetAttribute -SubnetId $SubnetA.SubnetId -MapPublicIpOnLaunch $true
New-EC2Tag -Resource $SubnetA.SubnetId -Tag $tag
New-EC2Tag -Resource $SubnetA.SubnetId -Tag $tagb
$SubnetB = New-EC2Subnet -VpcId $newvpc.VpcId -CidrBlock $ipbsubnet -AvailabilityZone $azlist[1].ZoneName
Edit-EC2SubnetAttribute -SubnetId $SubnetB.SubnetId -MapPublicIpOnLaunch $true
New-EC2Tag -Resource $SubnetB.SubnetId -Tag $tag
New-EC2Tag -Resource $SubnetB.SubnetId -Tag $tagb

$inetgw = New-EC2InternetGateway
New-EC2Tag -Resource $inetgw.InternetGatewayId -Tag $tag
New-EC2Tag -Resource $inetgw.InternetGatewayId -Tag $tagb
Add-EC2InternetGateway -InternetGatewayId $inetgw.InternetGatewayId -VpcId $newvpc.VpcId

$rtable = Get-EC2RouteTable -Filter @{ Name="vpc-id"; Value=$newvpc.VpcId }
Register-EC2RouteTable -RouteTableId $rtable[0].RouteTableId -SubnetId $SubnetA.SubnetId
Register-EC2RouteTable -RouteTableId $rtable[0].RouteTableId -SubnetId $SubnetB.SubnetId
New-EC2Tag -Resource $rtable[0].RouteTableId -Tag $tag
New-EC2Tag -Resource $rtable[0].RouteTableId -Tag $tagb

Edit-EC2SubnetAttribute -SubnetId $SubNetA.SubnetId -MapPublicIpOnLaunch $true
Edit-EC2SubnetAttribute -SubnetId $SubNetB.SubnetId -MapPublicIpOnLaunch $true

New-EC2Route -RouteTableId $rtable[0].RouteTableId -DestinationCidrBlock 0.0.0.0/0 -GatewayId $inetgw.InternetGatewayId



