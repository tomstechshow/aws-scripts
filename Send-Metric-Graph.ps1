
# Startup
# Load AWS PowerShell Module and Connect to AWS
Write-Host "Import AWS Module"
Import-Module AWSPowerShell

$awskey = 'aws access key'
$awssecret = 'aws secret'
$region = 'us-east-2'
$instanceid = 'i-123412341324'

Set-AWSCredential -AccessKey $awskey -SecretKey $awssecret -StoreAs default
Set-DefaultAWSRegion -Region $region

$dim = New-Object 'Amazon.CloudWatch.Model.Dimension'
$dim.Name = 'InstanceId'
$dim.Value = $instanceid

$now =  get-date
$then = (get-date).AddHours(-8)

$data = Get-CWMetricStatistics -Namespace AWS/EC2 -Period 60 `
    -MetricName CPUUtilization -StartTime $then -EndTime $now  `
    -Statistic Maximum -Dimension $dim

[void][Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms.DataVisualization")

$chart = new-object System.Windows.Forms.DataVisualization.Charting.Chart
$chartarea = new-object system.windows.forms.datavisualization.charting.chartarea
$chartarea.axisy.title = "CPU Load"
$chartarea.axisy.minimum = 0
$chartarea.axisy.maximum = 100
$chartarea.axisx.interval = 5
$chart.width = 1000
$chart.Height = 500
$chart.Left = 40
$chart.top = 30
$chart.Name = "CPU Over Time"
$chart.ChartAreas.Add($chartarea)
$legend = New-Object system.Windows.Forms.DataVisualization.Charting.Legend
$chart.Legends.Add($legend)

$chart.Series.Add("CPU")
$chart.Series["CPU"].ChartType = [System.Windows.Forms.DataVisualization.Charting.SeriesChartType]::Line
$chart.Series["CPU"].borderwidth=3
Foreach ($item in $data.Datapoints) {
    $q=$item.timestamp.ToShortTimeString()
    $chart.Series["CPU"].Points.addxy($q,$item.maximum)
}
$filename = "C:\Users\Thomas Nelson\Documents\code\aws-scripts\cpugraph"+$currentDate+".png"
$chart.SaveImage($filename, "PNG")


$EmailFrom = "tom@tomstechshow.com"
$EmailTo = "tom@tomstechshow.com"
$Attachment = New-Object Net.Mail.Attachment($filename)
$Attachment.ContentDisposition.Inline = $True
$Attachment.ContentDisposition.DispositionType = "Inline"
$Attachment.ContentType.MediaType = "image/png"
$Attachment.ContentId = 'chart.jpg'
$Body = "
<html>
  <head></head>
  <body>
    <img src='cid:chart.jpg' />
  </body>
</html>"
$Subject = "CPU Graph"
$SMTPMessage = New-Object System.Net.Mail.MailMessage ($EmailFrom, $EmailTo,$Subject ,$Body)
$SMTPMessage.IsBodyHtml = $true
$SMTPMessage.Attachments.Add($Attachment)
$SMTPServer = "smtp.office365.com"
$SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer , 587 )
$SMTPClient.EnableSsl = $true
$SMTPClient.Credentials = New-Object System.Net.NetworkCredential ("email@mailhost.com", "password")
$SMTPClient.Send($SMTPMessage)

 

