Import-Module AWSPowerShell.NetCore
$AccessKey  = "access key"
$SecretKey  = "secret key"
$Region     = "us-west-1"
$Sgid= "sg-address"
$snid = "subnet-address"
$InstanceName = "MyInstance"
$InstanceType = 't2.micro'
$BillingKey = "Infrastructure"
$KeyName = "MyAWSKeyPair"

Set-AWSCredential -AccessKey $AccessKey -SecretKey $SecretKey -StoreAs default
Set-DefaultAWSRegion -Region $Region

$filter_platform = New-Object Amazon.EC2.Model.Filter -Property @{Name = "name"; Values = "amzn2-ami-hvm-2.0.2019????-x86_64-gp2"} 
$amilist = Get-EC2Image -Owner amazon, self -Filter $filter_platform
$amilist = $amilist | Sort-object -Property 'Description' -Descending

$tag0 = @{ key="Name"; Value=$InstanceName }
$tag1 = @{ key="Billing"; Value=$BillingKey }
$tagspec1 = new-object Amazon.EC2.Model.TagSpecification
$tagspec1.ResourceType = "instance"
$tagspec1.Tags.Add($tag0)
$tagspec1.Tags.Add($tag1)

$bdm = New-Object Amazon.Ec2.Model.BlockDeviceMapping
$bdm.DeviceName = '/dev/sda1'

$ebd = New-Object Amazon.Ec2.Model.EbsBlockDevice
$ebd.VolumeSize = '50'
$ebd.VolumeType = 'gp2'

$bdm.Ebs = $ebd

$inst1 = New-EC2Instance -ImageId $amilist[0].ImageId -MinCount 1 -MaxCount 1 -KeyName $KeyName `
             -SecurityGroupId $Sgid -SubnetId $snid -TagSpecification $tagspec1 `
             -InstanceType $InstanceType -BlockDeviceMapping $bdm
Start-Sleep 60

$reservation = New-Object 'collections.generic.list[string]'
$reservation.add($inst1.ReservationId)
$filter_reservation = New-Object Amazon.EC2.Model.Filter -Property @{Name = "reservation-id"; Values = $reservation}
$instid = (Get-EC2Instance -Filter $filter_reservation).Instances.instanceID

$status = Get-EC2InstanceStatus -InstanceId $instid -Verbose 
$status.Status
while ($status.status.status.value -ne "ok") {
    Start-Sleep 5
    $status = Get-EC2InstanceStatus -InstanceId $instid -Verbose
}
Write-Host ((Get-EC2instance -InstanceId $instid).Instances).PublicIpAddress