		$hostdomain = 'mydomain.com'
		$instancename = 'myinstance'
$awslb ='awseb-AWSEB-123412341234-us-west-2.elb.amazonaws.com'


$domain = $hostdomain+'.'
$hz = Get-R53hostedZones | Where-Object Name -eq $domain
$fullurl = $instancename+'.'+$domain
$rrs = New-Object Amazon.Route53.Model.ResourceRecordset
$rrs.Name = $fullurl
$rrs.Type = "CNAME"
$rrs.ResourceRecords = New-Object Amazon.Route53.model.ResourceRecord ($awslb)
$rrs.TTL = 300
$addrec = New-Object Amazon.Route53.Model.Change ('CREATE', $rrs)
Edit-R53ResourceRecordSet -HostedZoneId $hz.id -ChangeBatch_Change $addrec