#Create Load Balancer
$lb = New-ELB2LoadBalancer -Type application -Scheme internet-facing -IpAddressType ipv4 -Name 'Main-LB' -SecurityGroup $sg.GroupId -subnet $subnets.SubnetId
#Wait for Load Balancer to be Ready
$lbState = (Get-ELB2LoadBalancer -LoadBalancerArn $lb.LoadBalancerArn).State.Code.Value
While ($lbstate -ne 'active') {
    Write-Host -NoNewline "."
    Start-Sleep 10
    $lbState = (Get-ELB2LoadBalancer -LoadBalancerArn $lb.LoadBalancerArn).State.Code.Value
}

#Set Forward from Port 80 to 443
Write-Host 'Adding Listener'
$rc = New-Object Amazon.ElasticLoadBalancingV2.Model.RedirectActionConfig
$rc.Host       = '#{host}'
$rc.Path       = '/#{path}'
$rc.Port       = '443'
$rc.Protocol   = 'HTTPS'
$rc.Query      = '#{query}'
$rc.StatusCode = 'HTTP_301'

$ra = New-Object Amazon.ElasticLoadBalancingV2.Model.Action
$ra.Type = 'redirect'
$ra.RedirectConfig = $rc

$Listen80 = New-ELB2Listener -LoadBalancerArn $lb.LoadBalancerArn -Port 80 -Protocol 'http' -DefaultAction $ra

#Set No URL Match Response
$fixedResponse = New-Object Amazon.ElasticLoadBalancingV2.Model.FixedResponseActionConfig
$fixedResponse.ContentType = 'text/plain'
$fixedResponse.StatusCode = '503'

$act = New-Object Amazon.ElasticLoadBalancingV2.Model.Action
$act.FixedResponseConfig = $fixedResponse
$act.Order = 99
$act.Type = 'fixed-response'

$Listen1 = New-ELB2Listener -LoadBalancerArn $lb.LoadBalancerArn -Port 443 -Protocol "HTTPS" -DefaultAction $act  -Certificate $certModel -SslPolicy $sslPolicy







#Create First Target Group
$tg1 = New-ELB2TargetGroup -HealthCheckEnabled 1 -Port 80 -Protocol 'HTTP' -TargetType instance -VpcId $vpc.vpcid -Name "Test-Server1"
#Register Instance With Target Group
$Reg1 = Register-ELB2Target -TargetGroupArn $tg1.TargetGroupArn -Target @{Port = 80; Id = 'i-1234567890ABCDEF'}

#Set Action to forward traffic to Target Group
$rule1 = New-Object Amazon.ElasticLoadBalancingV2.Model.RuleCondition
$rule1.field = 'host-header'
$rule1.values = 'test-server1-lb.tomstechshow.com'

$action1 = New-Object Amazon.ElasticLoadBalancingV2.Model.Action
$action1.TargetGroupArn = $tg1.TargetGroupArn
$action1.Type = "Forward"

New-Elb2Rule -Condition $rule1 -Action $action1 -Priority 1 -ListenerArn $Listen1.ListenerArn