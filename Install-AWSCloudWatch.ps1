$inst = ($vm.Instances).InstanceID
$inst = 'i-01c15925b56f1973a'





$awsagent = Send-SSMCommand -InstanceId @($inst) -DocumentName AWS-ConfigureAWSPackage -Parameter @{'action'='Install';'name'='AmazonCloudWatchAgent';'version'=''}
$instStatus = Get-SSMCommand -CommandId $awsagent.CommandId
while ($instStatus.status.value -ne "Success") {
    Start-Sleep 5
    $instStatus = Get-SSMCommand -CommandId $awsagent.CommandId
    if ($instStatus.status.value -eq "Failed")  {
         $instStatus.status.value
         Write-Host
         Break
    }
    Write-Host $instStatus.status.value
}

$awsagent = Send-SSMCommand -InstanceId @($inst) -DocumentName AmazonCloudWatch-ManageAgent -Parameter @{'action'='configure';'mode'='ec2';'optionalConfigurationSource'='ssm';'optionalConfigurationLocation'='AmazonCloudWatch-windows';'optionalRestart'='yes'}
$instStatus = Get-SSMCommand -CommandId $awsagent.CommandId
while ($instStatus.status.value -ne "Success") {
    Start-Sleep 5
    $instStatus = Get-SSMCommand -CommandId $awsagent.CommandId
    if ($instStatus.status.value -eq "Failed")  {
         $instStatus.status.value
         Break
    }
    $instStatus.status.value
}