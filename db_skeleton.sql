-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for cloudmgr
CREATE DATABASE IF NOT EXISTS `cloudmgr` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cloudmgr`;

-- Dumping structure for table cloudmgr.aws
CREATE TABLE IF NOT EXISTS `aws` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(50) NOT NULL,
  `AccessKey` char(50) NOT NULL,
  `SecretKey` char(50) NOT NULL,
  `Region` char(20) NOT NULL,
  `Bucket` char(100) NOT NULL,
  `CertARN` char(120) NOT NULL,
  `AlarmARN` char(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table cloudmgr.backups
CREATE TABLE IF NOT EXISTS `backups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_IX` int(11) NOT NULL DEFAULT '0',
  `name` char(50) NOT NULL,
  `datestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `region` char(50) NOT NULL,
  `ami` char(50) NOT NULL,
  `snap` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table cloudmgr.environments
CREATE TABLE IF NOT EXISTS `environments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(50) NOT NULL,
  `AWS_IX` char(12) DEFAULT NULL,
  `VPCID` char(50) DEFAULT NULL,
  `UseDefaultVPC_YN` char(50) DEFAULT NULL,
  `IPRange` char(50) DEFAULT NULL,
  `SubnetID0` char(50) DEFAULT NULL,
  `SubnetID1` char(50) DEFAULT NULL,
  `SubnetID2` char(50) DEFAULT NULL,
  `SubnetID3` char(50) DEFAULT NULL,
  `SubnetID4` char(50) DEFAULT NULL,
  `dns` char(50) DEFAULT NULL,
  `H_Status_ST` char(12) DEFAULT 'Configure',
  `VPCGroupID` char(50) DEFAULT NULL,
  `CreateDB_YN` char(50) DEFAULT NULL,
  `DBName` char(255) DEFAULT NULL,
  `DBConnection` char(255) DEFAULT NULL,
  `H_Active_YN` char(12) DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table cloudmgr.software
CREATE TABLE IF NOT EXISTS `software` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(50) NOT NULL,
  `Version` char(50) NOT NULL,
  `DeployTo_TO` char(50) NOT NULL,
  `Type_SO` char(50) NOT NULL,
  `Source` char(255) NOT NULL,
  `Parameters` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table cloudmgr.system
CREATE TABLE IF NOT EXISTS `system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(50) NOT NULL,
  `Environment_IX` char(50) NOT NULL,
  `Sizing_IT` char(50) NOT NULL,
  `OperatingSystem_OS` char(50) NOT NULL,
  `Software_IX` char(50) NOT NULL,
  `Software2_IX` char(50) NOT NULL DEFAULT 'NONE',
  `Software3_IX` char(50) NOT NULL DEFAULT 'NONE',
  `Subnet_SN` char(50) NOT NULL,
  `InstanceID` char(50) NOT NULL,
  `IP_Address` char(50) NOT NULL,
  `Priv_IP_Address` char(50) NOT NULL,
  `Password` char(50) NOT NULL,
  `Status` char(50) DEFAULT NULL,
  `awsdns` char(255) DEFAULT NULL,
  `Backup_YN` char(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table cloudmgr.usertable
CREATE TABLE IF NOT EXISTS `usertable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(50) DEFAULT NULL,
  `Password` char(50) DEFAULT NULL,
  `EMail` char(125) DEFAULT NULL,
  `Access` char(24) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
