Clear-Host
Set-Location -Path C:\xampp\htdocs\scripts
Set-ExecutionPolicy -Scope Process -ExecutionPolicy bypass
Import-Module MySQL
$dbpass = ConvertTo-SecureString -String 'awsadmin' -AsPlainText -Force
$dbuser = 'webuser'
$dbcred = New-Object -TypeName 'System.Management.Automation.PSCredential' -ArgumentList $dbuser, $dbpass
Connect-MySqlServer -Credential $dbCred -ComputerName 'localhost' -Database 'AWS'
Import-Module AWSPowerShell
$aws = 1
$aws_sql = 'Select * From aws where id = ' + $aws
$aws_settings = Invoke-MySqlQuery -query $aws_sql
Set-AWSCredential -AccessKey $aws_settings.AccessKey -SecretKey $aws_settings.SecretKey -StoreAs default


$regionlist = "us-west-2"
$certcheck = "arn:aws:acm:us-west-2:account:certificate/GUID"

foreach ($region in $regionlist) {
    Set-DefaultAWSRegion -Region $region
    Write-Host $region
    Write-Host '==========================='

    $elb2list = Get-ELB2LoadBalancer  
    foreach ($elb2 in $elb2list) {
        $listener = Get-ELB2Listener -LoadBalancerArn $elb2.LoadBalancerArn
        $flag = $false
        foreach ($check in $certcheck) {
            if ($check -eq $listener.Certificates.CertificateArn) {
                $flag = $true
            }
        }
        if ($flag -eq $true) {
            Write-Host $elb2.LoadBalancerName " " $listener.Certificates.CertificateArn -BackgroundColor DarkGreen -ForegroundColor White -NoNewline
            Write-Host "" -BackgroundColor Black -ForegroundColor White
        } else {
            Write-Host $elb2.LoadBalancerName " " $listener.Certificates.CertificateArn -BackgroundColor Black -ForegroundColor White
        }
    }

    $elblist = Get-ELBLoadBalancer  
    foreach ($elb in $elblist) {
        $flag = $false
        foreach ($check in $certcheck) {
            if ($check -eq $elb.ListenerDescriptions[0].Listener.SSLCertificateId) {
                $flag = $true
            }
        }
        if ($flag -eq $true) {
            Write-Host $elb.LoadBalancerName " " $elb.ListenerDescriptions[0].Listener.SSLCertificateId -BackgroundColor DarkGreen -ForegroundColor White -NoNewline
            Write-Host "" -BackgroundColor Black -ForegroundColor White
        } else {
            Write-Host $elb.LoadBalancerName " " $elb.ListenerDescriptions[0].Listener.SSLCertificateId -BackgroundColor Black -ForegroundColor White
        }
    }
    Write-Host ' '
}
Write-Host 'DONE' -BackgroundColor DarkGreen -ForegroundColor White -NonewLine
