Set-Location -Path "E:\s3" # Set the Path to store files
$accesskey = "sdfsdf"      # Set your AWS Access Key
$secret = "sdfsdf"         # Set your AWs Secret
$awsregion = "us-west-2"   # Set the Region of the Bucket

Import-Module AWSPowerShell
Set-AWSCredential -AccessKey $accesskey -SecretKey $secret -StoreAs default
Set-DefaultAWSRegion -Region $awsregion

$bucketlist = Get-S3Bucket | Where-Object -FilterScript { (Get-S3BucketLocation -BucketName $_.BucketName).value -eq $awsregion }

foreach ($bucket in $bucketlist.BucketName) {
    New-Item -Name $bucket -Type Directory
    Set-Location $bucket
    $items = Get-S3Object -BucketName $bucket
    foreach ($ikey in $items.key) { 
        Read-S3Object -BucketName $bucket -Key $ikey -File $ikey
    }
	Set-Location '..'
}

