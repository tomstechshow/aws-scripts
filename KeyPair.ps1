Import-Module AWSPowerShell.NetCore
$AccessKey  = "access-key"
$SecretKey  = "secret-key"
$Region     = "us-west-1"

Set-AWSCredential -AccessKey $AccessKey -SecretKey $SecretKey -StoreAs default
Set-DefaultAWSRegion -Region $Region

$keyname = 'MyAWSKeyPair'
$keypath = '/home/thomas/Documents/MyAWSKeyPair.pem'
$keypair = New-EC2KeyPair -KeyName $keyname
$keypair.KeyMaterial | Out-File -Encoding ascii $keypath
